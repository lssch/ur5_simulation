import numpy as np
from pytransform3d.urdf import UrdfTransformManager
import pytransform3d.visualizer as pv

from Robot import RobotKinematics

np.set_printoptions(precision=8)
np.set_printoptions(suppress=True)


def animation_callback(step, kinematics, tm, graph, joint_names, H, trajectory):
    for joint_name, angle in zip(joint_names, kinematics.qs[step]):
        tm.set_joint(joint_name, angle)

    H[step] = kinematics.base_to_ee(kinematics.qs[step])

    trajectory.set_data(H)
    graph.set_data()

    return graph, trajectory


if __name__ == '__main__':
    r = np.array([[0, 0, 0.089159],
                  [0, 0.13585, 0],
                  [0, -0.1197, 0.425],
                  [0, 0, 0.39225],
                  [0, 0.093, 0],
                  [0, 0, 0.09465],
                  [0, 0.0823, 0]])

    robot_kinematics = RobotKinematics(r)

    X_desired = np.array([0.4, 0.4, 0.4, 0.1, 0.2, 0.3])

    X_calculated = robot_kinematics.inverse(X_desired, 1e-3, 9.0, 0, 1e-3, False)

    print(f'X_desired: {X_desired}')
    print(f'X_calculated: {X_calculated}')

    # TODO: Get the .dae visualisation module to run, to show the robot with the actual colors
    with open('ur5_description/ur5_stl.urdf') as description:
        tm = UrdfTransformManager()
        tm.load_urdf(description.read(), 'ur5_description')

        # TODO: The parser automatically detects links an joints which start with 'link' 'joint'.
        #  For this to work, the urdf file must be rewritten.
        joint_names = [
            'shoulder_pan_joint',
            'shoulder_lift_joint',
            'elbow_joint',
            'wrist_1_joint',
            'wrist_2_joint',
            'wrist_3_joint'
        ]
        frame_names = [
            'base_link',
            'shoulder_link',
            'upper_arm_link',
            'forearm_link',
            'wrist_1_link',
            'wrist_2_link',
            'wrist_3_link',
            'ee_link'
        ]

        fig = pv.figure()
        graph = fig.plot_graph(tm, "base_link", s=0.2, show_frames=True, show_visuals=True)

        H = np.empty((len(robot_kinematics.qs), 4, 4))

        H[:] = robot_kinematics.base_to_ee(robot_kinematics.qs[0])
        trajectory = pv.Trajectory(H, n_frames=0, s=0.2, c=(0, 0, 0))
        trajectory.add_artist(fig)

        fig.view_init()
        fig.set_zoom(2.0)

        fig.animate(animation_callback, robot_kinematics.iterations, loop=True,
                    fargs=(robot_kinematics, tm, graph, joint_names, H, trajectory))

        fig.show()
