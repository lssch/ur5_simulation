import numpy as np
import matplotlib.pyplot as plt

Rx = lambda phi: np.array([[1, 0, 0],
                           [0, np.cos(phi), -np.sin(phi)],
                           [0, np.sin(phi), np.cos(phi)]])

Ry = lambda phi: np.array([[np.cos(phi), 0, np.sin(phi)],
                           [0, 1, 0],
                           [-np.sin(phi), 0, np.cos(phi)]])

Rz = lambda phi: np.array([[np.cos(phi), -np.sin(phi), 0],
                           [np.sin(phi), np.cos(phi), 0],
                           [0, 0, 1]])

T = lambda rot, trans, angle: np.vstack((np.hstack((rot(angle), trans.reshape(-1, 1))), np.array([0, 0, 0, 1])))

euler = lambda R: np.array([np.arctan2(R[1, 0], R[0, 0]),
                            np.arctan2(-R[2, 0], np.sqrt(R[2, 1] ** 2 + R[2, 2] ** 2)),
                            np.arctan2(R[2, 1], R[2, 2])])


class RobotKinematics(object):
    def __init__(self, r: np.ndarray):
        self.r = r
        self.q = np.random.rand(6) * 2 * np.pi
        self.qs = np.empty((0, self.q.shape[0]))
        self.iterations = 0

    def base_to_ee(self, q: np.ndarray) -> np.ndarray:
        # Calculate the forward kinematics transformation matrices for each joint
        Base_T_J1 = T(Rz, self.r[0], q[0])
        J1_T_J2 = T(Ry, self.r[1], np.pi / 2 + q[1])
        J2_T_J3 = T(Ry, self.r[2], q[2])
        J3_T_J4 = T(Ry, self.r[3], np.pi / 2 + q[3])
        J4_T_J5 = T(Rz, self.r[4], q[4])
        J5_T_J6 = T(Ry, self.r[5], q[5])
        J6_T_EE = T(Rz, self.r[6], np.pi / 2)

        # Compute the transformation from the base to the end effector
        return Base_T_J1 @ J1_T_J2 @ J2_T_J3 @ J3_T_J4 @ J4_T_J5 @ J5_T_J6 @ J6_T_EE

    def jacobian(self, q: np.ndarray) -> np.ndarray:
        # Calculate the forward kinematics transformation matrices for each joint
        Base_T_J1 = T(Rz, self.r[0], q[0])
        J1_T_J2 = T(Ry, self.r[1], np.pi / 2 + q[1])
        J2_T_J3 = T(Ry, self.r[2], q[2])
        J3_T_J4 = T(Ry, self.r[3], np.pi / 2 + q[3])
        J4_T_J5 = T(Rz, self.r[4], q[4])
        J5_T_J6 = T(Ry, self.r[5], q[5])
        J6_T_EE = T(Rz, self.r[6], np.pi / 2)

        # Compute the transformation from the base to the end effector
        Base_T_EE = Base_T_J1 @ J1_T_J2 @ J2_T_J3 @ J3_T_J4 @ J4_T_J5 @ J5_T_J6 @ J6_T_EE

        n = np.array([np.array([0, 0, 1]),
                      np.array([0, 1, 0]),
                      np.array([0, 1, 0]),
                      np.array([0, 1, 0]),
                      np.array([0, 0, 1]),
                      np.array([0, 1, 0])])

        # Compute the individual transfromation to each individual frame
        I_T = np.array([Base_T_J1,
                        Base_T_J1 @ J1_T_J2,
                        Base_T_J1 @ J1_T_J2 @ J2_T_J3,
                        Base_T_J1 @ J1_T_J2 @ J2_T_J3 @ J3_T_J4,
                        Base_T_J1 @ J1_T_J2 @ J2_T_J3 @ J3_T_J4 @ J4_T_J5,
                        Base_T_J1 @ J1_T_J2 @ J2_T_J3 @ J3_T_J4 @ J4_T_J5 @ J5_T_J6])

        I_n = np.array([I_T[i][0:3, 0:3] @ n[i] for i in range(len(n))])
        I_r = np.array([Base_T_EE[0:3, 3] - I_T[i][0:3, 3] for i in range(len(I_T))])
        J = np.array([np.hstack((np.cross(I_n[i], I_r[i][:3]).T, I_n[i])) for i in range(len(I_n))]).T
        return J

    def forward(self, q: np.ndarray) -> (np.ndarray, np.ndarray):
        Base_T_EE = self.base_to_ee(q)

        X_E_ypr = np.array([np.arctan2(Base_T_EE[1, 0], Base_T_EE[0, 0]),
                            np.arctan2(-Base_T_EE[2, 0], np.sqrt(Base_T_EE[2, 1] ** 2 + Base_T_EE[2, 2] ** 2)),
                            np.arctan2(Base_T_EE[2, 1], Base_T_EE[2, 2])])

        X_E = np.hstack((Base_T_EE[0:3, 3].T,
                         X_E_ypr.T))
        J = self.jacobian(q)

        return X_E, J

    def inverse(self, X_desired: np.ndarray, delta_t: float, linear_gain: float, angular_gain: float,
                prec: float = 1e-3, verbose: bool = False) -> np.ndarray:
        self.iterations = 0
        qs = np.empty((0, self.q.shape[0]))
        qs = np.vstack([qs, self.q])

        while True:
            self.iterations += 1
            X, J = self.forward(self.q)
            W = np.hstack([linear_gain * (X_desired[0:3] - X[0:3]),
                           angular_gain * (X_desired[3:6] - X[3:6])])
            d_q = np.linalg.pinv(J) @ W

            if verbose:
                print(f'It {self.iterations}: X_curr: {X}, d_q = {d_q}')

            self.q = self.q + d_q * delta_t

            qs = np.vstack([qs, self.q])

            if np.all(np.isclose(X_desired[0:3] - X[0:3], 0.0, atol=prec)):
                break

        self.qs = qs
        print(f'Inverse calculation took {self.iterations} iterations.')

        return X
