# URDF SIMULATION IN PYTHON

Platform independent solution for simulation URDF described robots and animate them using native gpu support.
This project relies heavily on the [pytransform3d](https://github.com/dfki-ric/pytransform3d?tab=readme-ov-file).
There are many [examples](https://dfki-ric.github.io/pytransform3d/_auto_examples/index.html) and good [documentation](https://dfki-ric.github.io/pytransform3d/api.html).

![Preview video](/doc/Preview.mp4)  